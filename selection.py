print("Loading Selection")

import pcbnew
from lib import *

selection = []

class SelectionRememberPlugin(pcbnew.ActionPlugin):

    def defaults(self):
        self.name = "Remember Selection"
        self.category = "layout"
        self.description = "Remembers current selection to be recalled later"

    def Run(self):
        board = pcbnew.GetBoard()
        
        # check selected pads too, should include footprint pad belongs to
        for module in board.GetModules():
            if(module.IsSelected()):
                selection.append(module)
                print(module.IsSelected())

        print('Saving selection: %s' % len(selection))

class SelectionRestorePlugin(pcbnew.ActionPlugin):

    def defaults(self):
        self.name = "Restore Selection"
        self.category = "layout"
        self.description = "Restores previous selection"

    def Run(self):
        # check selected pads too, should include footprint pad belongs to
        for module in selection:
            print(module)
            print(module.IsSelected())
            module.SetSelected()
            print(module.IsSelected())

        print('Restoring selection: %s' % len(selection))

SelectionRememberPlugin().register()
SelectionRestorePlugin().register()
