#!/usr/bin/env python3

import re
import sys
from enum import Enum
import pcbnew

class QueryType(Enum):
    MOD = 1

class ValueType(Enum):
    VAL = 1
    REF = 2


def query(board, type, val_type, value):
    ret = []

    pattern = re.compile(value)

    if type == QueryType.MOD:
        for module in board.GetModules():
            if(val_type == ValueType.VAL and module.GetValue() == value):
                ret.append(module)
            elif(val_type == ValueType.REF and pattern.match(module.GetReference())):
                ret.append(module)

    return ret

def SelectionCenter(selection):
    i = 0
    x = 0
    y = 0

    for module in selection:
        pos = module.GetPosition()

        x += pos.x
        y += pos.y

        i += 1

    x /= i
    y /= i

    return [ x, y ]
