if(__name__ == "__main__"):
    print("Run from CLI")
else:
    print("Run from UI")

class FlipHorizPlugin(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = "Flip Horizontally"
        self.category = "layout"
        self.description = "Swaps position of all selected components along their common center line"

    def Run(self):
        print("Run")

        board = pcbnew.GetBoard()
        
        selection = []

        # check selected pads too, should include footprint pad belongs to
        for module in board.GetModules():
            if(module.IsSelected()):
                selection.append(module)

        center = SelectionCenter(selection)

        for module in selection:
            pos = module.GetPosition()
            pos.x = center[0] + (center[0] - pos.x * 2)

            module.SetPosition(pos)
            module.Flip(pos)

FlipHorizPlugin().register() # Instantiate and register to Pcbnew

